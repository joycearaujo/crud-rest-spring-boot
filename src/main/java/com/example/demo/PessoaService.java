package com.example.demo;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {
	
	@Autowired
	PessoaRepository pr;
	
	public Pessoa atualizar(Long id, Pessoa pessoa) {
		Pessoa pSalva = buscaPessoaPorId(id);
		BeanUtils.copyProperties(pessoa, pSalva, "id");
		return pr.save(pSalva);
	}

	private Pessoa buscaPessoaPorId(Long id) {
		Optional<Pessoa> pSalva = pr.findById(id);
		if(pSalva.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return pSalva.get();
	}
	
	
	
}

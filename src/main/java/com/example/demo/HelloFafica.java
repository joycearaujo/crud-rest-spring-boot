package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/pessoas")
public class HelloFafica {
	
	@Autowired
	private PessoaRepository pr;
	
	@GetMapping("/get")
	public List<Pessoa> listarPessoa(){
		return pr.findAll();	
	}
	
	@GetMapping("/get/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Pessoa listarPessoa(@PathVariable Long id){
		return pr.findById(id).get();
	}
	
	@PostMapping("/post")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Pessoa> inserirPessoa(@RequestBody Pessoa pessoa){
		Pessoa pSalva = pr.save(pessoa);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(pSalva.getId()).toUri();
		return ResponseEntity.created(uri).body(pSalva);
	}
	
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletar(@PathVariable Long id) {
		pr.deleteById(id);
	}
	
	@PutMapping("/put/{id}")
	public ResponseEntity<Pessoa> atualizar(@PathVariable Long id, @Valid @RequestBody Pessoa pessoa){
		Optional<Pessoa> pSalva = pr.findById(id);
		if (!pSalva.isPresent())
			return ResponseEntity.notFound().build();
		BeanUtils.copyProperties(pessoa, pSalva.get(), "id");
		pr.save(pSalva.get());
		return ResponseEntity.ok(pSalva.get());
	}


}

